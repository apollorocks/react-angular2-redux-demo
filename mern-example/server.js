var fileStream = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var mongoClient = require('mongodb').MongoClient;

var app = express();

var mongoConnectionString = "mongodb://localhost:27017/mean-example";

setListeningPort();

setStaticPaths();

setBodyParser();

configureResponseToAllRequests();

addApiHandlers();

listenForRequests();

function setListeningPort() {
  app.set('port', (process.env.PORT || 3000));
}

function setStaticPaths() {
  app.use('/', express.static(path.join(__dirname, 'public')));
  app.use('/', express.static(path.join(__dirname, 'node_modules')));
}

function setBodyParser() {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));
}

function configureResponseToAllRequests() {
  // Additional middleware which will set headers that we need on each request.
  app.use(function(req, res, next) {
      // Set permissive CORS header - this allows this server to be used only as
      // an API server in conjunction with something like webpack-dev-server.
      res.setHeader('Access-Control-Allow-Origin', '*');

      // Disable caching so we'll always get the latest comments.
      res.setHeader('Cache-Control', 'no-cache');
      next();
  });
}

function addApiHandlers() {
  app.get('/api/todo', apiToDoHandler);
  app.post('/api/todo', apiToDoPostHandler)
}

function listenForRequests() {
  app.listen(app.get('port'), function() {
    console.log('Server started: http://localhost:' + app.get('port') + '/');
  });
}

function apiToDoPostHandler(req, res) {
  var todoItem = req.body;
  mongoClient.connect(mongoConnectionString, onConnected);

  function onConnected(err, db) {
    if(err) {
      reportError(err);
      return;
    }

    createTodoItem(db);
  }

  function createTodoItem(db) {
    var collection = db.collection("todo");
    collection.insert(todoItem, {w:1}, onCreateTodoItemCompleted);
  }

  function onCreateTodoItemCompleted(err, result) {
    if (err) {
      reportError(err);
      return;
    }

    if (result.result !== null && result.result.ok && result.result.n === 1 && result.ops !== null && result.ops.length > 0) {
      todoItem = result.ops[0];
    }
    else {
      reportError("Could not add")
      return;
    }

    res.json(todoItem);
  }
}

function apiToDoHandler(req, res) {
  mongoClient.connect(mongoConnectionString, onConnected);

  function onConnected(err, db) {
    if(err) {
      reportError(err);
      return;
    }

    fetchAllTodos(db);
  }

  function fetchAllTodos(db) {
    var todoCollection = db.collection("todo");
    todoCollection.find().toArray(onFetchAllTodosCompleted);
  }

  function onFetchAllTodosCompleted(err, todos) {
    if (err) {
      reportError(err);
      return;
    }

    res.json(todos);
  }

  function reportError(err) {
    res.status(500);
    res.send(err);
    console.log(err);
  }

  function reportSuccess(message) {
    res.json({message: message})
    console.log(message);
  }
}

// var COMMENTS_FILE = path.join(__dirname, 'comments.json');

// app.get('/api/comments', function(req, res) {
//   fileStream.readFile(COMMENTS_FILE, function(err, data) {
//     if (err) {
//       console.error(err);
//       process.exit(1);
//     }
//     res.json(JSON.parse(data));
//   });
// });

// app.post('/api/comments', function(req, res) {
//   fileStream.readFile(COMMENTS_FILE, function(err, data) {
//     if (err) {
//       console.error(err);
//       process.exit(1);
//     }
//     var comments = JSON.parse(data);
//     // NOTE: In a real implementation, we would likely rely on a database or
//     // some other approach (e.g. UUIDs) to ensure a globally unique id. We'll
//     // treat Date.now() as unique-enough for our purposes.
//     var newComment = {
//       id: Date.now(),
//       author: req.body.author,
//       text: req.body.text,
//     };
//     comments.push(newComment);
//     fileStream.writeFile(COMMENTS_FILE, JSON.stringify(comments, null, 4), function(err) {
//       if (err) {
//         console.error(err);
//         process.exit(1);
//       }
//       res.json(comments);
//     });
//   });
// });