class TodoService {
    getTodoItems(onSuccess) {
        return $.get("api/todo", onSuccess);
    }

    createTodoItem(todoItem, onSuccess) {
        var request = {
            url:"api/todo",      
            dataType: 'json',
            type: 'POST',
            data: todoItem,
            success: onSuccess
        }
        return $.ajax(request);
    }
}

class ToDoPage extends React.Component {
    constructor(props, context, updater) {
        super(props, context, updater);
        this.state = {
            todoItems: [], 
            isLoadingTodoItems: false,
            selectedFolderName: 'All'
        }
    }

    componentDidMount() {
        this.getAllTodoItems();
    }

    getAllTodoItems() {
        this.notifyLoadingTodoItems();

        new TodoService().getTodoItems(this.onGetAllTodoItemsCompleted.bind(this));
    }

    notifyLoadingTodoItems() {
        this.state.isLoadingTodoItems = true;
        this.setState(this.state);
    }

    onGetAllTodoItemsCompleted(result) {
        this.state.todoItems = result;
        this.state.isLoadingTodoItems = false;
        this.setState(this.state);
    }

    onTodoItemCreated(todoItem) {
        this.state.todoItems.push(todoItem);
        this.setState(this.state);
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-12">
                    <div>
                        <FoldersPane onTodoItemCreated={this.onTodoItemCreated.bind(this)}/>
                    </div>
                    <div>
                        {this.state.isLoadingTodoItems ? <span>Loading...</span> : null}
                        <ListPane selectedFolderName={this.state.selectedFolderName} todoItems={this.state.todoItems}/>
                    </div>
                </div>
            </div>
        )
    }
}

class FoldersPane extends React.Component {
    constructor(props, context, updater) {
        super(props, context, updater);

        this.state = {
            isAddingNewTodoItem: false
        }
    }

    onAddTodoItem() {
        this.showTodoItemForm();
    }

    onTodoItemSaved(todoItem) {
        this.closeAddTodoItemForm();
        this.notifyOnTodoItemCreated(todoItem);
    }

    notifyOnTodoItemCreated(todoItem) {
        if (!this.props.onTodoItemCreated) {
            return;
        }

        this.props.onTodoItemCreated(todoItem);
    }

    onTodoItemCanceled() {
        this.closeAddTodoItemForm();
    }

    showTodoItemForm() {
        this.state.isAddingNewTodoItem = true;
        this.setState(this.state);
    }

    closeAddTodoItemForm() {
        this.state.isAddingNewTodoItem = false;
        this.setState(this.state);
    }

    render() {
        return (
            <div>
                <div style={{marginBottom: "5px"}}>
                    <span className="btn btn-default" style={{marginRight: "5px"}}>Home</span>
                    <span className="btn btn-default" style={{marginRight: "5px"}}>Work</span>
                    <span className="btn btn-default" style={{marginRight: "5px"}}>Fun</span>
                    <button className="btn btn-default" onClick={this.onAddTodoItem.bind(this)}>Add Todo</button>
                </div>
                {
                    this.state.isAddingNewTodoItem ? 
                    <TodoDetails
                        todoItem={{folderName: "All", name: "", description: ""}} 
                        onTodoItemSaved={this.onTodoItemSaved.bind(this)} 
                        onTodoItemCanceled={this.onTodoItemCanceled.bind(this)}
                    /> 
                    : null
                }
            </div>
        )
    }
}

class ListPane extends React.Component {
    constructor(props, context, updater) {
        super(props, context, updater);
    }

    generateHeaderRow() {
        return (
            <thead>
                <tr>
                    <th>{this.props.selectedFolderName}</th>
                </tr>
            </thead>
        )
    }

    generateItemRows() {
        var itemRows = [];
        if (this.props.todoItems === null) {
            return null;
        }

        this.props.todoItems.forEach(function(todoItem) {
            itemRows.push(
                <TodoRow key={todoItem._id} todoItem={todoItem} />
            )
        })

        return (
                <tbody>
                {itemRows}
                </tbody>
        )
    }

    render() {
        return (
            <table className="table table-bordered table-striped">
                {this.generateHeaderRow()}
                {this.generateItemRows()}
            </table>
        )
    }
}

class TodoRow extends React.Component {
    constructor(props, context, updater) {
        super(props, context, updater);
    }

    render() {
        var todoItem = this.props.todoItem;
        return (
            <tr>
                <td>{todoItem.name}</td>
            </tr>
        )
    }
}

class TodoDetails extends React.Component {
    constructor(props, context, updater) {
        super(props, context, updater);
        this.state = {};
        this.state.todoItem = props.todoItem;
    }

    onSave() {
        var todoService = new TodoService();
        todoService.createTodoItem(this.state.todoItem, this.onCreateTodoItemCompleted.bind(this))
    }

    onCreateTodoItemCompleted(result) {
        this.state.todoItem = result;
        
        this.applyStateChanges();

        this.notifyTodoItemSaved();
    }

    notifyTodoItemSaved(todoItem) {
        if (!this.props.onTodoItemSaved) {
            return;
        }

        this.props.onTodoItemSaved(this.state.todoItem);
    }

    onCancel() {
        if (!this.props.onTodoItemCanceled) {
            return;
        }

        this.props.onTodoItemCanceled();
    }

    onFolderNameChanged(event) {
        this.state.todoItem.folderName = event.target.value;
        this.applyStateChanges();
    }

    onNameChanged(event) {
        this.state.todoItem.name = event.target.value;
        this.applyStateChanges();
    }

    onDescriptionChanged(event) {
        this.state.todoItem.description = event.target.value;
        this.applyStateChanges();
    }

    applyStateChanges() {
        this.setState(this.state);
    }

    render() {
        return (
            <form>
                <div className="form-group">
                    <label>Folder</label>
                    <input type="text" className="form-control" placeholder="FolderName" 
                        value={this.state.todoItem.folderName} 
                        onChange={this.onFolderNameChanged.bind(this)}
                    />
                </div>
                <div className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" placeholder="Task Name" 
                        value={this.state.todoItem.name} 
                        onChange={this.onNameChanged.bind(this)}
                    />
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <textarea rows="6" className="form-control" placeholder="Description"
                        value={this.state.todoItem.description} 
                        onChange={this.onDescriptionChanged.bind(this)}
                    />
                </div>
                <div className="pull-right">
                    <span className="btn btn-default" onClick={this.onSave.bind(this)} style={{marginRight: "5px"}}>Save</span>
                    <span className="btn btn-default" onClick={this.onCancel.bind(this)}>Cancel</span>
                </div>
            </form>
            )
    }
}

ReactDOM.render(
    <ToDoPage/>,
    document.getElementById('content')
);
