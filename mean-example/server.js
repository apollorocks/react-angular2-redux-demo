var fileStream = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');

var serverConfig = require("./serverConfig");

var taskGetTasksApi =require('./apis/taskGetTasksApi').taskGetTasksApi; 
var taskGetTaskByIdApi =require('./apis/taskGetTaskByIdApi').taskGetTaskByIdApi; 
var taskCreateTaskApi =require('./apis/taskCreateTaskApi').taskCreateTaskApi;
var taskUpdateTaskApi =require('./apis/taskUpdateTaskApi').taskUpdateTaskApi;
var taskDeleteTaskByIdApi =require('./apis/taskDeleteTaskByIdApi').taskDeleteTaskByIdApi;

var app = express();

setListeningPort();

setStaticPaths();

setBodyParser();

configureResponseToAllRequests();

addApiHandlers();

listenForRequests();

function setListeningPort() {
  app.set('port', (process.env.PORT || 3000));
}

function setStaticPaths() {
  app.use('/', express.static(path.join(__dirname, 'public')));
  app.use('/modules', express.static(path.join(__dirname, 'node_modules')));
}

function setBodyParser() {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));
}

function configureResponseToAllRequests() {
  // Additional middleware which will set headers that we need on each request.
  app.use(function(req, res, next) {
      // Set permissive CORS header - this allows this server to be used only as
      // an API server in conjunction with something like webpack-dev-server.
      res.setHeader('Access-Control-Allow-Origin', '*');

      // Disable caching so we'll always get the latest comments.
      res.setHeader('Cache-Control', 'no-cache');
      next();
  });
}

function listenForRequests() {
  app.listen(app.get('port'), function() {
    console.log('Server started: http://localhost:' + app.get('port') + '/');
  });
}

function addApiHandlers() {
  app.get('/api/task/', taskGetTasksApi);
  app.get('/api/task/:id', taskGetTaskByIdApi);
  app.post('/api/task', taskCreateTaskApi)
  app.put('/api/task', taskUpdateTaskApi)
  app.del('/api/task/:id', taskDeleteTaskByIdApi)
}