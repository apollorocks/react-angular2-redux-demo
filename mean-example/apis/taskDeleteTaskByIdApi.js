var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var serverConfig = require("../serverConfig");
var mongoConnectionString = serverConfig.mongoConnectionString;

function taskDeleteTaskByIdApi(req, res) {
  var id = req.params.id;

  if (id === undefined || id === "" || id === null) {
    reportError("id is not specified");
    return;
  }

  mongoClient.connect(mongoConnectionString, onConnected);

  function onConnected(err, db) {
    if(err) {
      reportError(err);
      return;
    }

    deleteTodoById(db);
  }

  function deleteTodoById(db) {
    var todoCollection = db.collection("todo");
    todoCollection.remove({_id: new ObjectId(id)}, {w:1}, 
      function(err, result) {
        onDeleteTodoByIdCompleted(err, result);
        db.close();
      });
  }

  function onDeleteTodoByIdCompleted(err, result) {
    if (err) {
      reportError(err);
      return;
    }
    res.status(200);

    res.json({ok:1});
  }

  function reportError(err) {
    res.status(500);
    res.send(err);
    console.log(err);
  }

  function reportSuccess(message) {
    res.json({message: message})
    console.log(message);
  }
}

module.exports.taskDeleteTaskByIdApi = taskDeleteTaskByIdApi;