var extend = require('util')._extend;
var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var serverConfig = require("../serverConfig");
var mongoConnectionString = serverConfig.mongoConnectionString;

function taskUpdateTaskApi(req, res) {
  var todoItem = req.body;
  if (todoItem === undefined) {
    reportError("No todo item detected");
    return;
  }

  mongoClient.connect(mongoConnectionString, onConnected);

  function onConnected(err, db) {
    if(err) {
      reportError(err);
      return;
    }

    updateTodoItem(db);
  }

  function updateTodoItem(db) {
    var todoItemCopy = extend({}, todoItem);
    delete todoItemCopy["_id"];

    var collection = db.collection("todo");

    collection.update({_id: new ObjectId(todoItem._id)}, todoItemCopy, {w:1}, 
        function(err, result) {
            onUpdateTodoItemCompleted(err, result);
            db.close();
        });
  }

  function onUpdateTodoItemCompleted(err, result) {
    if (err) {
      reportError(err);
      return;
    }

    if (!(result.result !== null && result.result.ok && result.result.n === 1)) {
      reportError({message: "Could not update", details: err})
      return;
    }

    res.json(todoItem);
  }

  function reportError(err) {
    res.status(500);
    res.send(err);
    console.log(err);
  }
}

module.exports.taskUpdateTaskApi = taskUpdateTaskApi;