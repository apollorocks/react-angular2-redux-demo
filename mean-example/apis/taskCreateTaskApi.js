var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var serverConfig = require("../serverConfig");
var mongoConnectionString = serverConfig.mongoConnectionString;

function taskCreateTaskApi(req, res) {
  var todoItem = req.body;
  if (todoItem === undefined) {
    reportError("No todo item detected");
    return;
  }

  todoItem._id = undefined;

  mongoClient.connect(mongoConnectionString, onConnected);

  function onConnected(err, db) {
    if(err) {
      reportError(err);
      return;
    }

    createTodoItem(db);
  }

  function createTodoItem(db) {
    var collection = db.collection("todo");
    collection.insert(todoItem, {w:1}, 
    function(err, result) { 
        onCreateTodoItemCompleted(err, result);
        db.close();
    });
  }

  function onCreateTodoItemCompleted(err, result) {
    if (err) {
      reportError(err);
      return;
    }

    if (result.result !== null && result.result.ok && result.result.n === 1 && result.ops !== null && result.ops.length > 0) {
      todoItem = result.ops[0];
    }
    else {
      reportError("Could not add")
      return;
    }

    res.json(todoItem);
  }

  function reportError(err) {
    res.status(500);
    res.send(err);
    console.log(err);
  }
}

module.exports.taskCreateTaskApi = taskCreateTaskApi;