var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var serverConfig = require("../serverConfig");
var mongoConnectionString = serverConfig.mongoConnectionString;

function taskGetTasksApi(req, res) {
  mongoClient.connect(mongoConnectionString, onConnected);

  function onConnected(err, db) {
    if(err) {
      reportError(err);
      return;
    }

    fetchAllTodos(db);
  }

  function fetchAllTodos(db) {
    var todoCollection = db.collection("todo");
    todoCollection.find().toArray(
        function(err, todos) {
            onFetchAllTodosCompleted(err, todos);
            db.close();
        });
  }

  function onFetchAllTodosCompleted(err, todos) {
    if (err) {
      reportError(err);
      return;
    }

    res.json(todos);
  }

  function reportError(err) {
    res.status(500);
    res.send(err);
    console.log(err);
  }

  function reportSuccess(message) {
    res.json({message: message})
    console.log(message);
  }
}

module.exports.taskGetTasksApi = taskGetTasksApi;