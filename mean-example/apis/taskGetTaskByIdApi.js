var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var serverConfig = require("../serverConfig");
var mongoConnectionString = serverConfig.mongoConnectionString;

function taskGetTaskByIdApi(req, res) {
  var id = req.params.id;

  if (id === undefined || id === "" || id === "0") {
    reportError("id is not specified");
    return;
  }

  mongoClient.connect(mongoConnectionString, onConnected);

  function onConnected(err, db) {
    if(err) {
      reportError(err);
      return;
    }

    fetchAllTodos(db);
  }

  function fetchAllTodos(db) {
    var todoCollection = db.collection("todo");
    todoCollection.find({_id: new ObjectId(id)}).toArray(
        function(err, todos) {
            onFetchAllTodosCompleted(err, todos);
            db.close();
        });
  }

  function onFetchAllTodosCompleted(err, todos) {
    if (err) {
      reportError(err);
      return;
    }
    var todo = {}
    if (todos.length > 0) {
      todo = todos[0];
    }

    res.json(todo);
  }

  function reportError(err) {
    res.status(500);
    res.send(err);
    console.log(err);
  }

  function reportSuccess(message) {
    res.json({message: message})
    console.log(message);
  }
}

module.exports.taskGetTaskByIdApi = taskGetTaskByIdApi;