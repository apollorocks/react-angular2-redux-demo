import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig} from 'angular2/router';
import {HTTP_PROVIDERS} from 'angular2/http';
import {TaskListComponent} from './Tasks/TaskListComponent';
import {TaskDetailComponent} from './Tasks/TaskDetailComponent';
import {TaskService} from  "./Tasks/TaskService";

@Component({
    selector: "app-todo",
    directives: [ROUTER_DIRECTIVES, TaskListComponent, TaskDetailComponent],
    providers: [HTTP_PROVIDERS, TaskService],
    template: `
        <div>
            <h1>{{pageTitle}}</h1>
            <nav>
                <a [routerLink]="['Home']">Home</a>
                | <a [routerLink]="['Tasks']">Tasks</a>
                | <a [routerLink]="['Task', {id: 0}]">Add Task</a>
                <router-outlet></router-outlet>
            </nav>
        </div>
    `
})
@RouteConfig([
    { path: "/", name: "Home", component: TaskListComponent, useAsDefault: true },
    { path: "/Tasks", name: "Tasks", component: TaskListComponent},
    { path: "/Task/:id", name: "Task", component: TaskDetailComponent}
])
export class AppTodoComponent {
    pageTitle: string = "Todo";
}