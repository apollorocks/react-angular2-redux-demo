import { bootstrap } from 'angular2/platform/browser';
import {provide} from 'angular2/core';
import {ROUTER_PROVIDERS, RouteConfig , ROUTER_DIRECTIVES, LocationStrategy, HashLocationStrategy} from  'angular2/router';

// Our main component
import { AppTodoComponent } from './AppTodoComponent';

bootstrap(AppTodoComponent, [ROUTER_PROVIDERS,  provide(LocationStrategy, {useClass: HashLocationStrategy})]);