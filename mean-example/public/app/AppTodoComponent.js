System.register(['angular2/core', 'angular2/router', 'angular2/http', './Tasks/TaskListComponent', './Tasks/TaskDetailComponent', "./Tasks/TaskService"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, http_1, TaskListComponent_1, TaskDetailComponent_1, TaskService_1;
    var AppTodoComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (TaskListComponent_1_1) {
                TaskListComponent_1 = TaskListComponent_1_1;
            },
            function (TaskDetailComponent_1_1) {
                TaskDetailComponent_1 = TaskDetailComponent_1_1;
            },
            function (TaskService_1_1) {
                TaskService_1 = TaskService_1_1;
            }],
        execute: function() {
            AppTodoComponent = (function () {
                function AppTodoComponent() {
                    this.pageTitle = "Todo";
                }
                AppTodoComponent = __decorate([
                    core_1.Component({
                        selector: "app-todo",
                        directives: [router_1.ROUTER_DIRECTIVES, TaskListComponent_1.TaskListComponent, TaskDetailComponent_1.TaskDetailComponent],
                        providers: [http_1.HTTP_PROVIDERS, TaskService_1.TaskService],
                        template: "\n        <div>\n            <h1>{{pageTitle}}</h1>\n            <nav>\n                <a [routerLink]=\"['Home']\">Home</a>\n                | <a [routerLink]=\"['Tasks']\">Tasks</a>\n                | <a [routerLink]=\"['Task', {id: 0}]\">Add Task</a>\n                <router-outlet></router-outlet>\n            </nav>\n        </div>\n    "
                    }),
                    router_1.RouteConfig([
                        { path: "/", name: "Home", component: TaskListComponent_1.TaskListComponent, useAsDefault: true },
                        { path: "/Tasks", name: "Tasks", component: TaskListComponent_1.TaskListComponent },
                        { path: "/Task/:id", name: "Task", component: TaskDetailComponent_1.TaskDetailComponent }
                    ]), 
                    __metadata('design:paramtypes', [])
                ], AppTodoComponent);
                return AppTodoComponent;
            }());
            exports_1("AppTodoComponent", AppTodoComponent);
        }
    }
});
//# sourceMappingURL=AppTodoComponent.js.map