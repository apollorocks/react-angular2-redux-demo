export class Task {
    folderName: string;
    name: string;
    description: string;
    _id: string;

    constructor(folderName: string = "", name: string = "", description: string = "", id: string = "") {
        this.folderName = folderName;
        this.name = name;
        this.description = description;
        this._id = id;
    }
}