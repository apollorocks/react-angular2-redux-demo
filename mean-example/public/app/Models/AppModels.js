System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Task;
    return {
        setters:[],
        execute: function() {
            Task = (function () {
                function Task(folderName, name, description, id) {
                    if (folderName === void 0) { folderName = ""; }
                    if (name === void 0) { name = ""; }
                    if (description === void 0) { description = ""; }
                    if (id === void 0) { id = ""; }
                    this.folderName = folderName;
                    this.name = name;
                    this.description = description;
                    this._id = id;
                }
                return Task;
            }());
            exports_1("Task", Task);
        }
    }
});
//# sourceMappingURL=AppModels.js.map