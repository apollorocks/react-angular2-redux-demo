import {Injectable} from 'angular2/core';
import {Task} from '../Models/AppModels';
import {Http, Response, Headers, RequestOptions} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx'

@Injectable()
export class TaskService {
    constructor(private _http: Http) {
    }

    getTasks(): Observable<Array<Task>> {
        return this._http.get("api/task")
        .map(this.onGetTasksCompleted.bind(this))
        .catch(this.onGetTasksError.bind(this))
    }

    onGetTasksCompleted(response: Response) {
        var tasks = response.json();
        return <Array<Task>>tasks;
    }

    onGetTasksError(error: Response, source: Observable<any>): Observable<any> {
        return source;
    }

    addTask(task: Task) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post("api/task", JSON.stringify(task), options)
            .map(this.onAddTaskCompleted.bind(this))
            .catch(this.onAddTaskError.bind(this));
    }
    

    onAddTaskCompleted(response: Response) {
        return <Task>response.json();
    }

    onAddTaskError(error) {

    }

    updateTask(task: Task) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.put("api/task", JSON.stringify(task), options)
            .map(this.onUpdateTaskCompleted.bind(this))
            .catch(this.onUpdateTaskError.bind(this));
    }

    onUpdateTaskCompleted(response: Response) {
        return <Task>response.json();
    }

    onUpdateTaskError(error) {

    }
    

    getTaskById(id: string) {
        return this._http.get("api/task/" + id)
        .map(this.onGetTaskCompleted.bind(this))
        .catch(this.onGetTaskError.bind(this))
    }

    onGetTaskCompleted(response: Response) {
        var task = response.json();
        return <Task>task;
    }

    onGetTaskError(error: Response, source: Observable<any>): Observable<any> {
        return source;
    }

    deleteTaskById(id: string) {
        return this._http.delete("api/task/" + id);
    }
}