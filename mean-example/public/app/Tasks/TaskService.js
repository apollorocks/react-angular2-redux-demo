System.register(['angular2/core', 'angular2/http', 'rxjs/Rx'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var TaskService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            TaskService = (function () {
                function TaskService(_http) {
                    this._http = _http;
                }
                TaskService.prototype.getTasks = function () {
                    return this._http.get("api/task")
                        .map(this.onGetTasksCompleted.bind(this))
                        .catch(this.onGetTasksError.bind(this));
                };
                TaskService.prototype.onGetTasksCompleted = function (response) {
                    var tasks = response.json();
                    return tasks;
                };
                TaskService.prototype.onGetTasksError = function (error, source) {
                    return source;
                };
                TaskService.prototype.addTask = function (task) {
                    var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
                    var options = new http_1.RequestOptions({ headers: headers });
                    return this._http.post("api/task", JSON.stringify(task), options)
                        .map(this.onAddTaskCompleted.bind(this))
                        .catch(this.onAddTaskError.bind(this));
                };
                TaskService.prototype.onAddTaskCompleted = function (response) {
                    return response.json();
                };
                TaskService.prototype.onAddTaskError = function (error) {
                };
                TaskService.prototype.updateTask = function (task) {
                    var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
                    var options = new http_1.RequestOptions({ headers: headers });
                    return this._http.put("api/task", JSON.stringify(task), options)
                        .map(this.onUpdateTaskCompleted.bind(this))
                        .catch(this.onUpdateTaskError.bind(this));
                };
                TaskService.prototype.onUpdateTaskCompleted = function (response) {
                    return response.json();
                };
                TaskService.prototype.onUpdateTaskError = function (error) {
                };
                TaskService.prototype.getTaskById = function (id) {
                    return this._http.get("api/task/" + id)
                        .map(this.onGetTaskCompleted.bind(this))
                        .catch(this.onGetTaskError.bind(this));
                };
                TaskService.prototype.onGetTaskCompleted = function (response) {
                    var task = response.json();
                    return task;
                };
                TaskService.prototype.onGetTaskError = function (error, source) {
                    return source;
                };
                TaskService.prototype.deleteTaskById = function (id) {
                    return this._http.delete("api/task/" + id);
                };
                TaskService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], TaskService);
                return TaskService;
            }());
            exports_1("TaskService", TaskService);
        }
    }
});
//# sourceMappingURL=TaskService.js.map