System.register(['angular2/core', 'angular2/router', '../Models/AppModels', './TaskService'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, AppModels_1, TaskService_1;
    var TaskDetailComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (AppModels_1_1) {
                AppModels_1 = AppModels_1_1;
            },
            function (TaskService_1_1) {
                TaskService_1 = TaskService_1_1;
            }],
        execute: function() {
            TaskDetailComponent = (function () {
                function TaskDetailComponent(_routerParam, _router, _taskService) {
                    this._routerParam = _routerParam;
                    this._router = _router;
                    this._taskService = _taskService;
                    this.task = new AppModels_1.Task();
                    this.isSaveAndNew = false;
                }
                TaskDetailComponent.prototype.ngOnInit = function () {
                    var id = this._routerParam.get("id");
                    if (id === "" || id === undefined || id == "0") {
                        return;
                    }
                    this.loadTask(id);
                };
                TaskDetailComponent.prototype.loadTask = function (id) {
                    var task = this._taskService.getTaskById(id)
                        .subscribe(this.onGetTaskCompleted.bind(this), this.onGetTasksError.bind(this));
                };
                TaskDetailComponent.prototype.onGetTaskCompleted = function (task) {
                    this.task = task;
                };
                TaskDetailComponent.prototype.onGetTasksError = function (error) {
                };
                TaskDetailComponent.prototype.onCancel = function () {
                    this.navigateToTasks();
                };
                TaskDetailComponent.prototype.onSave = function () {
                    this.isSaveAndNew = false;
                    this.onSaveOrUpdate();
                    //this.navigateToTasks();
                };
                TaskDetailComponent.prototype.onSaveNew = function () {
                    this.isSaveAndNew = true;
                    this.onSaveOrUpdate();
                };
                TaskDetailComponent.prototype.copyCurrentTaskAsNew = function () {
                    this.task = new AppModels_1.Task(this.task.folderName, this.task.name, this.task.description);
                };
                TaskDetailComponent.prototype.onSaveOrUpdate = function () {
                    if (this.isNewTask()) {
                        this.addNewTask();
                    }
                    else {
                        this.updateCurrentTask();
                    }
                };
                TaskDetailComponent.prototype.isNewTask = function () {
                    return this.task._id === "" || this.task._id === "0" || this.task._id === null;
                };
                TaskDetailComponent.prototype.addNewTask = function () {
                    this._taskService.addTask(this.task)
                        .subscribe(this.onAddTaskCompleted.bind(this), this.onAddTaskError.bind(this));
                };
                TaskDetailComponent.prototype.onAddTaskCompleted = function (task) {
                    this.task = task;
                    this.applySaveAndNew();
                };
                TaskDetailComponent.prototype.applySaveAndNew = function () {
                    if (this.isSaveAndNew) {
                        this.copyCurrentTaskAsNew();
                    }
                    else {
                        this.navigateToTasks();
                    }
                };
                TaskDetailComponent.prototype.onAddTaskError = function (error) {
                };
                TaskDetailComponent.prototype.updateCurrentTask = function () {
                    this._taskService.updateTask(this.task)
                        .subscribe(this.onUpdateTaskCompleted.bind(this), this.onUpdateTaskError.bind(this));
                };
                TaskDetailComponent.prototype.onUpdateTaskCompleted = function (task) {
                    this.task = task;
                    this.applySaveAndNew();
                };
                TaskDetailComponent.prototype.onUpdateTaskError = function (error) {
                };
                TaskDetailComponent.prototype.onDelete = function () {
                    if (this.task._id == "") {
                        return;
                    }
                    this._taskService.deleteTaskById(this.task._id)
                        .subscribe(this.navigateToTasks.bind(this), function () { alert("Could not delete task"); });
                };
                TaskDetailComponent.prototype.navigateToTasks = function () {
                    this._router.navigate(["Tasks"]);
                };
                TaskDetailComponent = __decorate([
                    core_1.Component({
                        selector: "task-detail",
                        templateUrl: "app/Tasks/TaskDetailComponent.html"
                    }), 
                    __metadata('design:paramtypes', [router_1.RouteParams, router_1.Router, TaskService_1.TaskService])
                ], TaskDetailComponent);
                return TaskDetailComponent;
            }());
            exports_1("TaskDetailComponent", TaskDetailComponent);
        }
    }
});
//# sourceMappingURL=TaskDetailComponent.js.map