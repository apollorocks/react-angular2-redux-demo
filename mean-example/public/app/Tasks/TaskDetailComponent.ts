import {Component, OnInit, AfterViewInit} from 'angular2/core';
import {RouteParams, Router} from 'angular2/router';

import {Task} from '../Models/AppModels';
import {TaskService} from './TaskService';

@Component({
    selector: "task-detail",
    templateUrl: "app/Tasks/TaskDetailComponent.html"
    /*
    template: `
        <div class="row-fluid">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Folder Name</label>
                    <input class="form-control" type="text" [(ngModel)]="task.folderName"/>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" type="text" [(ngModel)]="task.name"/>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" type="text" [(ngModel)]="task.description" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <span class="pull-left">
                        <span class="btn btn-default" (click)="onDelete()">Delete</span>
                    </span>
                    <span class="pull-right">
                        <span class="btn btn-default" (click)="onSave()">Save</span>
                        <span class="btn btn-default" (click)="onSaveNew()">Save &amp; New</span>
                        <span class="btn btn-default" (click)="onCancel()">Cancel</span>
                    </span>
                </div>
            </div>
        </div>
    `
    */
})
export class TaskDetailComponent implements OnInit {
    task: Task = new Task();

    isSaveAndNew = false;

    constructor(private _routerParam: RouteParams, private _router: Router, private _taskService : TaskService) {

    }

    ngOnInit() {
        let id = this._routerParam.get("id");
        if (id === "" || id === undefined || id == "0") {
            return;
        }

        this.loadTask(id);
    }

    loadTask(id: string) {
        var task = this._taskService.getTaskById(id)
            .subscribe(
                this.onGetTaskCompleted.bind(this), 
                this.onGetTasksError.bind(this));
    }

    onGetTaskCompleted(task: Task) {
        this.task = task;
    }

    onGetTasksError(error: any) {

    }

    onCancel() {
        this.navigateToTasks();
    }

    onSave() {
        this.isSaveAndNew = false;
        this.onSaveOrUpdate();
        //this.navigateToTasks();
    }

    onSaveNew() {
        this.isSaveAndNew = true;
        this.onSaveOrUpdate();
    }

    copyCurrentTaskAsNew() {
        this.task = new Task(this.task.folderName, this.task.name, this.task.description);
    }

    onSaveOrUpdate() {
        if (this.isNewTask()) {
            this.addNewTask();
        }
        else {
            this.updateCurrentTask();
        }
    }

    isNewTask() : boolean {
        return this.task._id === "" || this.task._id === "0" || this.task._id === null;
    }

    addNewTask() {
        this._taskService.addTask(this.task)
            .subscribe(
                this.onAddTaskCompleted.bind(this), 
                this.onAddTaskError.bind(this));
    }

    onAddTaskCompleted(task: Task) {
        this.task = task;
        this.applySaveAndNew();
    }

    applySaveAndNew() {
        if (this.isSaveAndNew) {
            this.copyCurrentTaskAsNew();
        }
        else {
            this.navigateToTasks();
        }
    }

    onAddTaskError(error) {

    }
    
    updateCurrentTask() {
        this._taskService.updateTask(this.task)
            .subscribe(
                this.onUpdateTaskCompleted.bind(this), 
                this.onUpdateTaskError.bind(this));
    }

    onUpdateTaskCompleted(task: Task) {
        this.task = task;
        this.applySaveAndNew();
    }

    onUpdateTaskError(error) {
    }

    onDelete() {
        if (this.task._id == "") {
            return;
        }
        this._taskService.deleteTaskById(this.task._id)
            .subscribe(
                this.navigateToTasks.bind(this), 
                function() {alert("Could not delete task");});
    }
    
    navigateToTasks() {
        this._router.navigate(["Tasks"]);
    }
}
