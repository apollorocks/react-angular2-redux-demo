import {Component, OnInit, AfterViewInit} from 'angular2/core';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig} from 'angular2/router';
import {Task} from '../Models/AppModels';
import {TaskService} from './TaskService';

@Component({
    selector: "task-list",
    directives: [ROUTER_DIRECTIVES],
    template: `
        <div>
            <table class="table table-striped">
                <tr><th></th><th>Folder</th><th>Name</th><th>Description</th></tr>
                <tr *ngFor="#task of tasks">
                    <td><a href="" [routerLink]="['Task', {id: task._id}]">Edit</a>
                    <td>{{task.folderName}}</td>
                    <td>{{task.name}}</td>
                    <td>{{task.description}}</td>
                </tr>
            </table>
        </div>
    `
})
export class TaskListComponent implements OnInit {
    tasks: Array<Task> = new Array<Task>();

    constructor(private _taskService: TaskService) {
    }

    ngOnInit() {
        this._taskService.getTasks()
            .subscribe(
                this.onGetTasksCompleted.bind(this), 
                this.onGetTasksError.bind(this));
    }

    onGetTasksCompleted(tasks: Task[]) {
        this.tasks = tasks;
    }

    onGetTasksError(error: any) {

    }
}
