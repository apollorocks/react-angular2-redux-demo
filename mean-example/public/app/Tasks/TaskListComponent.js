System.register(['angular2/core', 'angular2/router', './TaskService'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, TaskService_1;
    var TaskListComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (TaskService_1_1) {
                TaskService_1 = TaskService_1_1;
            }],
        execute: function() {
            TaskListComponent = (function () {
                function TaskListComponent(_taskService) {
                    this._taskService = _taskService;
                    this.tasks = new Array();
                }
                TaskListComponent.prototype.ngOnInit = function () {
                    this._taskService.getTasks()
                        .subscribe(this.onGetTasksCompleted.bind(this), this.onGetTasksError.bind(this));
                };
                TaskListComponent.prototype.onGetTasksCompleted = function (tasks) {
                    this.tasks = tasks;
                };
                TaskListComponent.prototype.onGetTasksError = function (error) {
                };
                TaskListComponent = __decorate([
                    core_1.Component({
                        selector: "task-list",
                        directives: [router_1.ROUTER_DIRECTIVES],
                        template: "\n        <div>\n            <table class=\"table table-striped\">\n                <tr><th></th><th>Folder</th><th>Name</th><th>Description</th></tr>\n                <tr *ngFor=\"#task of tasks\">\n                    <td><a href=\"\" [routerLink]=\"['Task', {id: task._id}]\">Edit</a>\n                    <td>{{task.folderName}}</td>\n                    <td>{{task.name}}</td>\n                    <td>{{task.description}}</td>\n                </tr>\n            </table>\n        </div>\n    "
                    }), 
                    __metadata('design:paramtypes', [TaskService_1.TaskService])
                ], TaskListComponent);
                return TaskListComponent;
            }());
            exports_1("TaskListComponent", TaskListComponent);
        }
    }
});
//# sourceMappingURL=TaskListComponent.js.map