//Install mongodb on osx following directions from url
//https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/

var mongoConnectionString = "mongodb://localhost:27017/mean-example";

module.exports.mongoConnectionString = mongoConnectionString;