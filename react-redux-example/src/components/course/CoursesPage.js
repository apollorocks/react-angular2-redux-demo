import React, {PropTypes} from 'react'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';//Why do I have to go up two levels?

class CoursesPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            course: {title: "apollo title"}
        }

        this.onTitleTextChanged = this.onTitleTextChanged.bind(this);
        this.onClickSave = this.onClickSave.bind(this);
        this.courseRow = this.courseRow.bind(this);
    }

    onTitleTextChanged(event) {
        const course = this.state.course;
        course.title = event.target.value;
        this.setState({course});
    }

    onClickSave() {
        this.props.actions.createCourseAction(this.state.course);
    }

    courseRow(course, index) {
        return (
            <div key={index}>{course.title}</div>
        );
    }

    render() {
        return (
            <div>
                <h1>Courses Page</h1>
                {this.props.courses.map(this.courseRow)}
                <h2>Add Course</h2>
                <input 
                    type="text"
                    onChange = {this.onTitleTextChanged}
                    value={this.state.course.title}
                />
                <input
                    type="submit"
                    value="Save"
                    onClick={this.onClickSave}
                />
            </div>
        );
    }
}

function mapReducersForProps(reducers, ownProps) {
    return {
        courses: reducers.courseReducer //This is a mess as far as readability
    }
}

function mapActionsToProps(dispatch) {
    return {
        actions: bindActionCreators(courseActions, dispatch)
    }
}

export default connect(mapReducersForProps, mapActionsToProps)(CoursesPage);