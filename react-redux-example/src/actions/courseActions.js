export function createCourseAction(course) {
    return {type: 'CREATE_COURSE', course}
}